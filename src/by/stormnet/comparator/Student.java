package by.stormnet.comparator;

public class Student {
    private int age;
    private String firstName;
    private String lastName;

    public Student(int age, String firstName, String lastName) {

        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }





    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        Integer o1 = new Integer(this.age);
//        Integer o2 = new Integer(o.getAge());
//        return o1.compareTo(o2);
//    }
}
