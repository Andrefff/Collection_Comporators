package by.stormnet.comparator;

import java.util.*;

public class Application {
    public static void main(String[] args) {
        Application app = new Application();
        app.runTheApp();
    }

    private void runTheApp() {
        Set<Student> students = new TreeSet<>(new AgeComparator().thenComparing(new NameComparator()));

        students.add(new Student(18,"Vasya","Pupkin1"));
        students.add(new Student(20,"Petia","Pupkin2"));
        students.add(new Student(19,"Petia","Pupkin3"));

        List<Student> studentList = new LinkedList<>(students);


        for(Student student:students){
            System.out.println(student);
        }
        System.out.println();

        for (Student student : studentList){
            System.out.println(student);
        }
        System.out.println();

        studentList.sort(new NameComparator().thenComparing(new AgeComparator()));
        for (Student student : studentList){
            System.out.println(student);
        }
    }
}
